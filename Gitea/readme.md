### Gitea Modern
| ![Gitea Modern Repository Page](https://codeberg.org/Freeplay/css-styles/raw/branch/main/images/codeberg-modern_repoPg.png) | ![Gitea Modern Explore Page](https://codeberg.org/Freeplay/css-styles/raw/branch/main/images/codeberg-modern_explorePg.png) | ![Gitea Modern Profile Page](https://codeberg.org/Freeplay/CSS-Styles/raw/branch/main/images/codeberg-modern_profilePg.png)
|---|---|---|

This style changes the layout of Gitea, and uses Gitea's color variables. It can be used with any theme that changes only colors.

### Disclaimer
**I don't know if everything is properly polished, and the style is still being worked on. If you find an issue with the style, please create an issue. Or if you can fix an issue yourself, create a pull request!**

### Usage as Gitea theme
To install the theme for all Gitea users:

1. Download the [`theme-gitea-modern.css` file][theme-file] and add it to your custom folder in `$GITEA_CUSTOM/public/css/` ([learn how to find that directory][doc-dir]).
  - The path has changed in Gitea 1.15, there the file needs to be placed in `$GITEA_CUSTOM/public/assets/css`
2. Adapt your `app.ini` to make it the default theme or be user-selectable:
    - To change the default theme, change the value of `DEFAULT_THEME` in the [ui section][doc-config] of `app.ini` to `gitea-modern`

    - To make a theme selectable by users in their profile settings, append `,gitea-modern` to the list of `THEMES` in your `app.ini`.
3. Restart Gitea
4. Enjoy :)


### Usage with Stylus
If you use the Stylus browser extension, you can use the [`theme-gitea-modern.user.css`][usertheme-file]) file to apply to any Gitea instance.

### Contributing
To make changes, always base them on [`theme-gitea-modern.styl`](theme-gitea-modern.styl), which is written for any CSS preprocessor that supports CSS nesting, like 'Stylus' (recommended) or 'Less'.
When making contributions, don't use the compiled version for your changes.

[theme-file]: https://codeberg.org/Freeplay/CSS-Styles/raw/branch/main/Gitea/theme-gitea-modern.css
[usertheme-file]: https://codeberg.org/Freeplay/CSS-Styles/raw/branch/main/Gitea/theme-gitea-modern.user.css
[doc-config]: https://docs.gitea.io/en-us/config-cheat-sheet/#ui-ui
[doc-dir]: https://docs.gitea.io/en-us/faq/#where-does-gitea-store-what-file
