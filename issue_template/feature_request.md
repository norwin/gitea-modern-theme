---

name: "Feature Request"
about: "Suggest something for one of my styles!"
title: "Style Name: A clear title!"
labels:
- enhancement

---

<!--
	Milestones are used to track which style this issue belongs to.
	Click the cog icon next to "Minestone" in the right sidebar
	Select the Style that this issue belongs to.
-->

**Link to file:**


**Describe the feature:**

---
- [x] I have made sure this issue hasn't already been posted.
- [x] I have made sure I am on the latest version of my Browser.
- [x] I have made sure that the platform I am running the style on is fully up-to-date.
