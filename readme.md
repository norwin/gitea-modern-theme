#### Click into folders to view styles for that site.

| [Icon from Lucide](https://lucide.dev/) | [**View UserStyles list**](https://codeberg.org/Freeplay/UserStyles) | `.user.css` files can be applied using the [Stylus](https://add0n.com/stylus.html) browser extension. |
|---|---|---|